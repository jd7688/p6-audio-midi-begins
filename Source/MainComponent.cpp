/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //Enable Midi Impulse Input(2 spaces between "Impulses").
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);
    
    //Set MainComponent as a midi listener.
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    
    midiLabel.setText("YAY!", dontSendNotification);
    addAndMakeVisible(midiLabel);
    
    DBG("Done!");
}

MainComponent::~MainComponent()
{
    //Stop main component being a midi listener on deletion of main component class.
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    midiLabel.setBounds(10, 10, getWidth() -20, 40);
}

void MainComponent::handleIncomingMidiMessage(MidiInput*, const MidiMessage& message)
{
    DBG ("Midi!");
    String midiText;
    
    //if (message.isNoteOnOrOff())
    //{
    //    midiText << "NoteOn: Channel " << message.getChannel();
    //    midiText << ":Number" << message.getNoteNumber();
    //    midiText << ":Velocity" << message.getVelocity();
    //}
    //midiLabel.getTextValue() = midiText;
}